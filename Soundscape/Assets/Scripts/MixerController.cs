﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MixerController : MonoBehaviour
{
    public AudioMixerSnapshot[] snapshots;
    private bool inside = false;
    public float transitionTime = 3f;

    // Update is called once per frame
    void Update()
    {
        if(inside)
        {
            snapshots[0].TransitionTo(transitionTime);
        }
        else
        {
            snapshots[1].TransitionTo(transitionTime);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        inside = !inside;
    }
}
