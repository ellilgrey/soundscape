﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemeController : MonoBehaviour
{
    public AudioSource source;
    public AudioClip clip;

    private float vol;

    void Start()
    {
        vol = 1f;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            source.PlayOneShot(clip, vol);
            vol = 0f;
        }
    }
}
