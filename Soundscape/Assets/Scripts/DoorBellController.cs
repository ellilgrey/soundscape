﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorBellController : MonoBehaviour
{
    //public variables
    public AudioSource source;
    public AudioClip clip;
    public float pitchMin;
    public float pitchMax;
    public float volMin;
    public float volMax;

    //private variables
    private float vol;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            source.pitch = Random.Range(pitchMin, pitchMax);
            vol = Random.Range(volMin, volMax);
            source.PlayOneShot(clip,vol);
        }
    }
}
