﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour
{
    private AudioSource source;
    public float pitchMin = 1f;
    public float pitchMax = 1.1f;
    void Awake()
    {
        source = GetComponent<AudioSource>();
    }
    void OnMouseDown()
    {
        source.pitch = Random.Range(pitchMin, pitchMax);
        source.PlayOneShot(source.clip, 1f);
    }
}
